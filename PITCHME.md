# Thema - Worum gehts?

Note:
Herausfinden der Verbreitung von IoT Devices in einem Netzwerk mittels IPv6 Scan.
Herausfinden, welches Scan-Tool besser geeignet ist für interne und externe Netzwerkscans. Definition von Merkmalen der IoT Geräte.
---

## Forschungsfragen
---

1. Can IoT devices in a network be discovered using IPv6 scanning?
2. How do IoT devices distinguish themselves from traditional network components when found with an IPv6 Scan?
3. What Operating Systems for IoT device classes do exist and are preferred choices when setting up a network?
4. Are there IPv6 addressing methods for IoT devices that can be applied in different device classes?
---

## Was gibt es schon?

Note:
IPv6, IPv6 für IoT Devices, 6LoWPAN (ressourcenarme IoT devices), IPv6 Addressierungsstrategien für IoT Netzwerke, OS für verschiedene IoT Device-Klassen, Sicherheitsstudien für IoT Schwachstellen
---

## Erkenntnisse - Zukünftige Arbeit

Note:
Es gibt sehr viele OS für IoT Devices, es werden nur ein paar ausgewählt für die künftige Arbeit.
Das Tool Shodan wird für IPv6 Scans verwendet werden und als Suchkriterium die ausgewählten IoT OS verwendet werden. Es wird versucht herauszufinden, inwieweit IoT Devices überhaupt auffindbar sind und welche Merkmale sie haben.
---

## Questions?
